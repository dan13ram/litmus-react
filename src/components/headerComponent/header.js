import React from 'react';
import { withRouter } from 'react-router-dom';
import LogoLink from './logoLinkComponent/logoLink';

const Header = (props) => {
    return (
        <header className="header">
            <div className="on-the-map">
                <span className="on-the-map-inner general-hover">ON THE MAP</span>
            </div>
            <div className="logo" >
                <LogoLink to="/" route={props.location.pathname}>litmus</LogoLink>
            </div>
            <div className="book-room" >
                <span className="book-room-inner general-hover">BOOK A ROOM</span>
            </div>
        </header>
    );
}

export default withRouter(Header);
