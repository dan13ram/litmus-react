import React from 'react';
import { Link } from 'react-router-dom';

class LogoLink extends React.Component {
    render () {
        if(this.props.route === this.props.to){
            return <span className="scroll-top">{this.props.children}</span>
        }
        return <Link to={this.props.to}>{this.props.children}</Link>
    }
}

export default LogoLink;
