import React from 'react';
import DetailsSection from './detailsSectionComponent/detailsSection';
import image1 from '../../../Assets/imgs/asset_1.jpg';
import image3 from '../../../Assets/imgs/asset_3.jpg';
import image5 from '../../../Assets/imgs/asset_5.jpg';
import image7 from '../../../Assets/imgs/asset_7.jpg';

const DetailsContent = ({type}) => {
    const mediumTitle = "NAKA _medium_";
    const mediumPhonetic = "/’mi:dIem/";
    const mediumDesc = "The intervening substance through which sensory impressions are conveyed or physical forces are transmitted.";
    const mediumBoldDesc = "Medium sized rooms are perfect for one person and their favourite miniature companion.";
    const catalystTitle = "SHOKUBAI _catalyst_";
    const catalystPhonetic = "/’kat(e)lIst/";
    const catalystDesc = "A substance that increases the rate of a chemical reaction without itself undergoing any permanent chemical change.";
    const catalystBoldDesc = "Experience a range of amenities with the Brewery, cinema and mall - just a few steps away.";

    if (type!=="catalyst") {
        return (
            <div className="details-content">
                <DetailsSection name="medium" title={mediumTitle} phonetic={mediumPhonetic} desc={mediumDesc} boldDesc={mediumBoldDesc} images={[image1, image7]}/>
                <DetailsSection name="catalyst" title={catalystTitle} phonetic={catalystPhonetic} desc={catalystDesc} boldDesc={catalystBoldDesc} images={[image3, image5]}/>
            </div>
        );
    } else {
        return (
            <div className="details-content">
                <DetailsSection name="catalyst" title={catalystTitle} phonetic={catalystPhonetic} desc={catalystDesc} boldDesc={catalystBoldDesc} images={[image3, image5]}/>
                <DetailsSection name="medium" title={mediumTitle} phonetic={mediumPhonetic} desc={mediumDesc} boldDesc={mediumBoldDesc} images={[image1, image7]}/>
            </div>
        );
    }
}

export default DetailsContent;
