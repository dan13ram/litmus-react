import React from 'react';

import SliderImages from './sliderImagesComponent/sliderImages';

const DetailsSection = (props) => {
    return (
        <div className={"details-section " + props.name}>
            <div className="container">
                <SliderImages images={props.images}/>
                <div className="desc-container">
                    <div className="description">
                        <h1>{props.title}</h1>
                        <p>{props.phonetic}<br/>
                            {props.desc}
                            <br/>
                            <br/>
                            <b>{props.boldDesc}</b></p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DetailsSection;
