import React from 'react';
import * as PIXI from "pixi.js";
import { Sprite, Stage, Container } from "react-pixi-fiber";
import { TimelineMax, Expo } from 'gsap/all';

import left from '../../../../../Assets/imgs/left.png';
import right from '../../../../../Assets/imgs/right.png';
import bump_image from '../../../../../Assets/imgs/slider_bump.jpg';

class HoverImage extends React.Component {
    constructor(props) {
        super(props);
        this.SPEED = 1.25;
        this.DISTORTION = 500;
        this.pixi = React.createRef();
        this.displacement_sprite = React.createRef();
        this.images = [];
        this.resize = this.resize.bind(this);
        this.displacement_texture = PIXI.Texture.from(bump_image);
        this.displacement_texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
        this.t = new TimelineMax();
        this.mouseClickNext = this.mouseClickNext.bind(this);
        this.updateCurrentAsNext = this.updateCurrentAsNext.bind(this);
        this.mouseClickPrevious = this.mouseClickPrevious.bind(this);
        this.updateCurrentAsPrevious = this.updateCurrentAsPrevious.bind(this);
        this.CURRENT = 0;
        this.numImages = this.props.images.length;
        this.alpha = { value: 0 };
        this.state = {
            width: 0,
            height: 0,
            filter: null,
            alpha: this.props.images.map((image, index) => { if (index === 0) return 1; else return 0; }),
        }

    }

    resize() {
        const filter = new PIXI.filters.DisplacementFilter(this.displacement_sprite.current);
        filter.scale.set(0);
        const newState = {
            width: this.pixi.current.offsetWidth,
            height: this.pixi.current.offsetHeight,
            filter: filter
        }
        this.setState(newState);
    }

    componentDidMount() {
        this.resize();
        window.addEventListener('resize', this.resize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resize);
    }

    mouseClickNext() {
        this.t
            .to(this.state.filter.scale, this.SPEED, {x: this.DISTORTION, y: 0, ease: Expo.easeIn, onComplete: this.updateCurrentAsNext});
        this.t
            .to(this.state.filter.scale, this.SPEED, {x: 0, y: 0, ease: Expo.easeOut});
    }
    updateCurrentAsNext() {
        this.CURRENT = this.CURRENT === this.numImages-1 ? 0 : this.CURRENT + 1;
        let newAlpha = this.state.alpha.map((value, index) => { if (index === this.CURRENT) return 1; else return 0; });
        this.setState({alpha: newAlpha});
    }
    mouseClickPrevious() {
        this.t
            .to(this.state.filter.scale, this.SPEED, {x: this.DISTORTION, y: 0, ease: Expo.easeIn, onComplete: this.updateCurrentAsPrevious});
        this.t
            .to(this.state.filter.scale, this.SPEED, {x: 0, y: 0, ease: Expo.easeOut});
    }
    updateCurrentAsPrevious() {
        this.CURRENT = this.CURRENT === 0? this.numImages-1 : this.CURRENT - 1;
        let newAlpha = this.state.alpha.map((value, index) => { if (index === this.CURRENT) return 1; else return 0; });
        this.setState({alpha: newAlpha});
        console.log(this.CURRENT, this.alpha);
    }
    render() {
        return (
            <div className="slide-container">
                <div className="left">
                    <img alt="left" src={left} onClick={this.mouseClickPrevious}/>
                </div>
                <div className={"slider-image images"} ref={this.pixi}>
                    <div className="slider-container">
                        <div className="slider-layover"/>
                        <Stage width={this.state.width} height={this.state.height} >
                            <Container filters={[this.state.filter]} >
                                { 
                                    this.props.images.map((image, index) => {
                                        return (
                                            <Sprite key={"slider-image-"+index} 
                                                ref={(input) => {this.images[0] = input }}
                                                height={this.state.height} 
                                                width={this.state.height * 2.67} 
                                                x={this.state.width/2}
                                                y={this.state.height/2}
                                                anchor={0.5}
                                                texture={PIXI.Texture.from(image)}
                                                alpha={this.state.alpha[index]}
                                            />
                                        );
                                    })
                                }
                            </Container>
                            <Sprite ref={this.displacement_sprite} width={this.state.width} height={this.state.height} texture={this.displacement_texture}/>
                        </Stage>
                    </div>
                </div>
                <div className="right">
                    <img alt="right" src={right} onClick={this.mouseClickNext}/>
                </div>
            </div>

        );
    }
}

export default HoverImage;
