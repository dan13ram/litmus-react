import React from 'react';

import DetailsContent from './detailsContentComponent/detailsContent.js';

const Details = ({ match }) => {
        return (
            <div className="details">
                <DetailsContent type={match.params.detailId || "medium"}/>
            </div>
        );
}

export default Details;
