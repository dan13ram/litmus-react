import React from 'react';

import Text from './textComponent/text';

class PrivacyPolicy extends React.Component{
    render() {
        return (
            <div className="privacy-policy">
                <Text/>
            </div>
        );
    }
}

export default PrivacyPolicy;
