import React from 'react';

const Text = () => {
    return (
        <div className="text-content">
            <div className="container">
                <h2>Hotel Policy</h2>
                <article>
                    <p><span className="square">&#9632;</span>Check-in time is 12:00 p.m. At the time of check-in, every individual guest staying in the hotel is required to present a valid government identity proof which mentions the residential address. In case of foreign national, valid passport along with visa is required. Guests are requested to carry the reservation letter at the time of arrival.</p>
                    <p><span className="square">&#9632;</span>Check-out time is 11:00 a.m. Late check-out is subjected to availability and it’s management discretion to charge additionally thereafter. </p>
                    <p><span className="square">&#9632;</span>Room keys are issued to the registered guest(s). No room keys will be issued to youth under 18 at any time. Valid identification is required if you have lost your key and require a duplicate. Please return your room keys to the front desk while you check-out.</p>
                    <p><span className="square">&#9632;</span>Litmus management shall not be responsible for any loss incurred during the tenure of a patron’s stay.</p>
                    <p><span className="square">&#9632;</span>The hotel holds the privilege to exercise its right of admission without assigning any reason - tough luck. Guest will be held responsible for any loss or damages to the hotel property caused by themselves, their buddies/guests or any person for whom they are responsible.</p>
                    <p><span className="square">&#9632;</span>Outside alcohol in the rooms is not allowed.</p>
                    <p><span className="square">&#9632;</span>Smoking in the hotel rooms, corridor, and staircase areas is strictly prohibited.</p>
                    <p><span className="square">&#9632;</span>No pets allowed (Apologies to all animal lovers)</p>
                    <p><span className="square">&#9632;</span>The breakfast buffet will be served from 7:00 to 10:00 a.m.</p>
                    <p><span className="square">&#9632;</span>Indulge in one of the many amenities at the mall.</p>
                </article>

            </div>
        </div>
    );
}

export default Text;
