import React from 'react';
import Logo from './logoComponent/logo';

const LandingPage = () => {
    return (
        <div className="landing-page">
            <Logo/>
        </div>
    );
}

export default LandingPage;
