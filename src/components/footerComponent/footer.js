import React from 'react';
import { NavLink } from 'react-router-dom';
import phone from '../../Assets/imgs/phone.svg';
import email from '../../Assets/imgs/email.svg';
import Social from '../socialComponent/social.js';

const Terms = () => { return ( <NavLink className="general-hover" activeClassName="current" to="/terms-and-conditions/" name="terms-and-conditions">Terms &amp; Conditions</NavLink> ) }
const Privacy = () => { return ( <NavLink className="general-hover" activeClassName="current" to="/privacy-policy/" name="privacy-policy">Privacy Policy</NavLink> ) }
const Hotel = () => { return ( <NavLink className="general-hover" activeClassName="current" to="/hotel-policy/" name="hotel-policy">Hotel Policy</NavLink> ) }
const Disclaimer = () => { return ( <NavLink className="general-hover" activeClassName="current" to="/disclaimer/" name="disclaimer">Disclaimer</NavLink> ) }

class Footer extends React.Component{
    componentDidMount() {
        document.querySelector(".map").addEventListener("click", function() {
            this.firstChild.classList.add('clicked');

        });
        document.querySelector(".map").addEventListener("mouseleave", function() {
            this.firstChild.classList.remove('clicked');
        });

    }
    render() {
        return (
            <footer>
                <div className="map">
                    <iframe title="google-maps-link" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d248842.53101866756!2d77.4534619!3d12.9613205!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3dd1c850d58f%3A0x63164cc542d95ab1!2sVaishnavi+Sapphire+Centre!5e0!3m2!1sen!2sin!4v1558851151697!5m2!1sen!2sin" width="100%" height="100%" allowFullScreen/>
                </div>

                <div className="footer-first">
                    <table className="contact">
                        <tbody>
                            <tr>
                                <td>
                                    <img alt="email" src={email} className="email-icon"/>
                                </td>
                                <td>
                                    reservations@litmushotels.in
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="phone" src={phone} className="phone-icon"/>
                                </td>
                                <td>
                                    080 2972 4422
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table className="location">
                        <tbody>
                            <tr><td>&#9632; Yeshwanthpur Metro Station</td><td></td><td className="distance">600 m</td></tr>
                            <tr><td>&#9632; MG Road</td><td></td><td className="distance">11 km</td></tr>
                            <tr><td>&#9632; Bangalore International Airport</td><td></td><td className="distance">34 km</td></tr>
                            <tr><td>&#9632; Bengaluru International Exhibition Centre</td><td></td><td className="distance">12 km</td></tr>
                        </tbody>
                    </table>
                    <Social/>
                </div>

                <div className="footer-second-container">
                    <div className="footer-second">
                        <div className="address">
                            <div className="inner">
                                Litmus Boutique Hotels
                                <br/>Sapphire Centre Mall,
                                <br/>Level 4, 9/36 Tumkur Road,
                                <br/>Yeshwantpur, Bangalore - 560 022
                            </div>
                        </div>
                        <div className="legal">
                            <div className="inner">
                                <Terms/>|<Privacy/>|<Hotel/>|<Disclaimer/>
                            </div>
                        </div>
                        <div className="copyright">
                            <div className="inner">
                                &copy; Litmus Hotels 2019, All Rights Reserved<br/>
                                Design by <a className="general-hover" href="https://www.spoilt.ideas.autopilot.co.in/" target="_blank" rel="noopener noreferrer">autopilot: Spoilt Ideas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
