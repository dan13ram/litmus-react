import React from 'react';
import Scrollbar from 'smooth-scrollbar';
import { TimelineMax, Expo } from 'gsap/all';
import Footer from '../footerComponent/footer'

const ScrollWrapper = (Inner) => {
    return class extends React.Component {
        constructor(props){
            super(props);
            this.isEnabled = !window.mobilecheck();
            this.scroll = React.createRef();
            this.scrollToTop = this.scrollToTop.bind(this);
            this.scrollClick = this.scrollClick.bind(this);
            this.scrollTopClick = this.scrollTopClick.bind(this);
            this.mapClick = this.mapClick.bind(this);
            this.imageTweens = {};
            this.textTweens = {};
        }
        scrollToTop() {
            this.scroll.current.scrollIntoView();
        }
        scrollTopClick() {
            if (this.isEnabled) {
                this.scrollbar.scrollTo(0, 0, 500);
            } else {
                document.querySelector('.home-page').scrollIntoView({behavior: "smooth"});
            }
        }
        scrollClick() {
            if (this.isEnabled) {
                this.scrollbar.scrollIntoView(document.querySelector(".medium"));
            } else {
                document.querySelector('.medium').scrollIntoView({behavior: "smooth"});
            }
        }
        mapClick() {
            if (this.isEnabled) {
                this.scrollbar.scrollIntoView(document.querySelector(".map"));
            } else {
                document.querySelector('.map').scrollIntoView({behavior: "smooth"});
            }
        }

        componentDidMount() {
            if (this.isEnabled) {
                this.scrollbar = Scrollbar.init(this.scroll.current);
            } else {
                this.scroll.current.style.height = "100%";
            }

            document.querySelector(".on-the-map").addEventListener("click", this.mapClick);
            if (this.props.location.pathname === "/") {

                const scrollButton = document.querySelector(".scroll");
                if (scrollButton !== null) {
                    scrollButton.addEventListener("click", this.scrollClick);
                }
                const scrollTop = document.querySelector(".scroll-top");
                if (scrollTop !== null) {
                    scrollTop.addEventListener("click", this.scrollTopClick);
                }
                if (this.isEnabled) {
                    document.querySelectorAll('.home-section').forEach((el) => {
                        const name = el.getAttribute('name');
                        const tween = new TimelineMax();
                        tween
                            .to("#"+name+"-image1", 5, {y: 0, ease: Expo.easeOut})
                            .to("#"+name+"-image2", 2, {y: 0, ease: Expo.easeOut}, 3)
                            .pause();
                        const textTween = new TimelineMax();
                        textTween
                            .to("#"+name+"-description", 1, {width: "48%", ease: Expo.easeIn})
                            .pause();
                        this.imageTweens[name] = tween;
                        this.textTweens[name] = textTween;
                    });

                    this.scrollbar.addListener((status) => {
                        document.querySelectorAll('.home-section').forEach((el) => {
                            const name = el.getAttribute('name');
                            let elY = el.getBoundingClientRect().y;
                            let height = window.innerHeight;
                            let elProgress = 1 - (elY / height);
                            if (elProgress >= 0 && elProgress <= 1) {
                                this.imageTweens[name].progress(elProgress);
                                if (this.scrollbar.isVisible(document.getElementById(name+"-description"))) {
                                    if (status.direction.y === 'down' && elProgress >= 0.8) {
                                        this.textTweens[name].play();
                                    } 
                                    if (status.direction.y === 'up' && elProgress <= 0.8) {
                                        this.textTweens[name].reverse();
                                    }
                                }
                            } else if (elProgress > 1 && elProgress <= 1.1) {
                                this.imageTweens[name].progress(1);
                            }
                        });
                    });
                } else {
                    document.querySelectorAll('.hover-image').forEach((el) => {
                        el.style.transform = "translate3d(0px, 0px, 0px)";
                    });
                    document.querySelectorAll('.desc-overlay').forEach((el) => {
                        el.style.display = "none";
                    });
                }
                if (this.props.lastLocation) {
                    let className = this.props.lastLocation.pathname.split('/');
                    if (className[1] === "details" && className[2] && className[2] !== "") {
                        let element = document.querySelector('.'+className[2])
                        let boundingRect = element.getBoundingClientRect();
                        if (this.isEnabled) {
                            this.scrollbar.setPosition(boundingRect.x, boundingRect.y);
                        } else {
                            setTimeout(function() { document.querySelector(".scroll-container").scrollIntoView(); element.scrollIntoView()}, 1000);
                        }
                    } else {
                        setTimeout(this.scrollToTop, 1000);
                    }
                }
            } else {
                if (this.props.lastLocation) {
                    setTimeout(this.scrollToTop, 1000);
                }
            }
        }
        componentWillUnmount() {
            document.querySelector(".on-the-map").removeEventListener("click", this.mapClick);
            const scrollButton = document.querySelector(".scroll");
            if (scrollButton !== null) {
                scrollButton.removeEventListener("click", this.scrollClick);
            }
            const scrollTop = document.querySelector(".scroll-top");
            if (scrollTop !== null) {
                scrollTop.removeEventListener("click", this.scrollTopClick);
            }
            if (this.isEnabled) {
                this.scrollbar.destroy();
            }
        }

        render() {
            return (
                <div className="scroll-wrapper">
                    <div className="page-overlay"/>
                    <div ref={this.scroll} className="scroll-container" data-scrollbar>
                        <Inner {...this.props}/>
                        <Footer/>
                    </div>
                </div>
            );
        }
    }
}

export default ScrollWrapper;
