import React from 'react';
import instagram from '../../Assets/imgs/instagram.svg';
import facebook from '../../Assets/imgs/facebook.svg';

const Social = () => {
    return (
        <table className="social">
            <tbody>
                <tr>
                    <td>
                        <img alt="instagram" src={instagram} className="instagram"/>
                    </td>
                    <td>
                        <img alt="facebook" src={facebook} className="facebook"/>
                    </td>
                </tr>
            </tbody>
        </table>
    );
}

export default Social
