import React from 'react';
import instagram from '../../Assets/imgs/instagram.svg';
import facebook from '../../Assets/imgs/facebook.svg';
import twitter from '../../Assets/imgs/twitter.svg';

const Social = () => {
    return (
        <table className="social">
            <tbody>
                <tr>
                    <td>
                        <img alt="instagram" src={instagram} className="instagram general-hover"/>
                    </td>
                    <td>
                        <img alt="facebook" src={facebook} className="facebook general-hover"/>
                    </td>
                    <td>
                        <img alt="twitter" src={twitter} className="twitter general-hover"/>
                    </td>
                </tr>
            </tbody>
        </table>
    );
}

export default Social
