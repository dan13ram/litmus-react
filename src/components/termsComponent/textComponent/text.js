import React from 'react';

const Text = () => {
    return (
        <div className="text-content">
            <div className="container">
                <h2>Terms and Conditions</h2>
                <article>
                    <h4>Damage To Hotel property</h4>
                    <p>Litmus Boutique Hotels reserves the right to charge guests the cost of rectifying the damage. Should this damage come to light after the guest has departed, we reserve the right to make a charge to the guest's credit / debit card, or send an invoice for the amount to the registered address. We will however make every effort to rectify any damage internally prior to contracting specialists to make the repairs, and therefore will make every effort to keep any costs that the guest would incur to a minimum</p>
                    <h4>Removal Of Hotel Property</h4>
                    <p>Litmus Boutique Hotels reserves the right to charge guests the cost of replacing any items that are removed from the premises by them without consent. The charge will be the full replacement amount of the missing item, including any carriage charges. Should the fact that the item is missing come to light after the guest has departed, we reserve the right to make a charge to the guests credit / debit card, or send an invoice for the amount to the registered address. Tampering With Fire Detection Systems &amp; Firefighting Equipment Litmus Boutique Hotels reserves the right to take action against any guest found to have tampered / interfered with any fire detection equipment throughout the hotel, including detector heads in public areas and bedrooms, break glass points and fire extinguishers. Guests found to have tampered with any fire detection or firefighting equipment will be charged with any costs incurred by the hotel due to their actions and additionally may be asked to leave the hotel. Depending on the severity of the guest actions, the Police may become involved at the hotels discretion. Should the fact that firefighting or detection equipment had been tampered with come to light after the guest has departed, we reserve the right to make a charge to the guests credit / debit card, or send an invoice for the amount to the registered address.</p>
                    <h4>Inappropriate Behaviour</h4>
                    <p>It is the hotels policy that all our guests have the right to be treated with dignity and respect and as a responsible host we believe that we have a duty to our guests to protect them from inappropriate behaviour. Should any actions by a guest be deemed inappropriate by the Duty Manager, or if any inappropriate behaviour is brought to the attention of the Duty Manager, the hotel reserves the right, after any allegations have been investigated, to take action against the guest. Depending on the severity of the guest actions, the Police may become involved at the hotels discretion, or guests may be asked to leave the hotel.</p>

                    <h4>Lost / Damaged Property</h4>
                    <p>Should any guest lose any belongings during their stay or incur damage to their property, the provision of the Hotel Proprietors Act 1956 will apply, a copy of which is displayed in reception. If we find any lost property, we will make every reasonable effort to locate the owner and return it, but if we cannot locate the owner and an item is not reclaimed with 3 months of the guest's departure it will be disposed of by the hotel.</p>

                    <h4>Payment Terms</h4>
                    <p>Please check the payment gateway terms &amp; conditions.</p>

                    <h4>Check In/Out Times</h4>
                    <p>Check-In Time is 12:00 p.m. and Check-Out Time is 11:00 a.m. Any extension to the check out time either pre-arranged or as a late departure, may or may not incur a charge upon departure.</p>

                    <h4>Vehicle Parking</h4>
                    <p>All vehicles are parked at the owner's risk. Should a problem occur with a vehicle in the hotel car park, the hotel cannot accept any liability. If a vehicle is left in the hotel car park for more than 8 hours after the guest has departed without the written consent of the hotel, the hotel reserves the right to remove the vehicle at the owners expense.</p>

                    <h4>Child Policy</h4>
                    <p>Litmus Boutique Hotels recognizes children as being 12 or under. Please be aware that individuals over 12 will be classed as adults and charged accordingly on arrival.</p>

                    <h4>Note</h4>
                    <p>Nothing in these terms &amp; conditions shall limit or restrict the guests legal rights as a consumer. Advice concerning such rights may be obtained from a Citizens Advice Bureau.</p>
                </article>

            </div>
        </div>
    );
}

export default Text;
