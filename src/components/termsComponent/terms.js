import React from 'react';

import Text from './textComponent/text';

class TermsAndConditions extends React.Component{
    render() {
        return (
            <div className="terms-and-conditions">
                <Text/>
            </div>
        );
    }
}

export default TermsAndConditions;
