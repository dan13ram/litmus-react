import React from 'react';

import Text from './textComponent/text';

class Disclaimer extends React.Component{
    render() {
        return (
            <div className="disclaimer">
                <Text/>
            </div>
        );
    }
}

export default Disclaimer;
