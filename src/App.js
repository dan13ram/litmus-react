import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { LastLocationProvider, withLastLocation } from 'react-router-last-location';

// components
import Header from './components/headerComponent/header'
import HomePage from './components/homePageComponent/homePage'
import Details from './components/detailsComponent/details'
import Terms from './components/termsComponent/terms'
import PrivacyPolicy from './components/privacyPolicyComponent/privacyPolicy'
import HotelPolicy from './components/hotelPolicyComponent/hotelPolicy'
import Disclaimer from './components/disclaimerComponent/disclaimer'
import ScrollWrapper from './components/scrollWrapper/scrollWrapper'

// css
import './Assets/css/default.min.css';

class App extends React.Component{

    render() {
        return (
            <BrowserRouter>
                <LastLocationProvider>
                    <Header/>
                    <Route render={({location}) => (
                        <TransitionGroup className="group">
                            <CSSTransition key={location.key} timeout={2200} classNames="slide">
                                <Switch location={location}>
                                    <Route path="/" exact component={withLastLocation(HomePage)} />
                                    <Route path="/details/:detailId?" component={withLastLocation(ScrollWrapper(Details))} />
                                    <Route path="/terms-and-conditions/" component={withLastLocation(ScrollWrapper(Terms))} />
                                    <Route path="/privacy-policy/" component={withLastLocation(ScrollWrapper(PrivacyPolicy))} />
                                    <Route path="/hotel-policy/" component={withLastLocation(ScrollWrapper(HotelPolicy))} />
                                    <Route path="/disclaimer/" component={withLastLocation(ScrollWrapper(Disclaimer))} />
                                </Switch>
                            </CSSTransition>
                        </TransitionGroup>
                    )}/>
            </LastLocationProvider>
        </BrowserRouter>
        );
    }
}

export default App;
